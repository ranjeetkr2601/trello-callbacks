const cards = require('./cards.json');

function callback2(listId, callbackFn){
    setTimeout(() => {
        if (typeof listId === "string"){
            if (!(listId in cards)){
                callbackFn(`${listId} not found in Cards`);
            }
            else {
               callbackFn(null, cards[listId]);
            }
        }
        else {
            callbackFn(new Error("Incorrect arguments passed"));
        }
    }, 2 * 1000);   
}

module.exports = callback2;