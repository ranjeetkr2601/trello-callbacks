
function callback1(boards, boardId, callBackFn) {
    setTimeout(() => {
        if (Array.isArray(boards) && typeof boardId === "string") {
            const foundBoard = boards.find((currentBoard) => {
                return currentBoard.id === boardId;
            });

            if (typeof foundBoard === "undefined") {
                callBackFn(`${boardId} not found`);
            } else {
                callBackFn(null, foundBoard);
            }
        }
        else {
            callBackFn(new Error("Incorrect Arguments passed"));
        }
    }, 2 * 1000);
}

module.exports = callback1;