const lists = require('./lists.json');
const boards = require('./boards.json');

function callback2(boardId, callbackFn){
    setTimeout(() => {
        if (typeof boardId === "string"){

            let foundId = boards.find((currentBoard) => {
                return currentBoard.id === boardId;
            });

            if (typeof foundId ==='undefined'){
                callbackFn(`${boardId} not found in Boards`);
            }
            else {
                if(!(foundId.id in lists)){
                    callbackFn(`${foundId} not found in lists`);
                }
                else {
                    callbackFn(null, lists[foundId.id]);
                }
            }
        }
        else {
            callbackFn(new Error("Incorrect arguments passed"));
        }
    }, 2 * 1000);   
}

module.exports = callback2;
