const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");
const boards = require('./boards.json');

function callback4(callback){
    setTimeout(() => {
        let foundBoard = boards.find((currentBoard) => {
            return currentBoard.name === "Thanos";
        });
        if(typeof foundBoard === 'undefined'){
            console.error("Board not found");
        }
        else {
            callback1(boards, foundBoard.id, (err, foundBoard) => {
                if(err){
                    callback(err);
                } else {
                    callback2(foundBoard.id, (err, foundList) => {
                        if(err){
                            callback(err);
                        }
                        else {
                            let foundMind = foundList.find((currentList) => {
                                return currentList.name === 'Mind';
                            });
                            if(typeof foundMind === 'undefined'){
                                console.error("List not found");
                            }
                            else {
                                callback3(foundMind.id, (err, foundcard) => {
                                    if(err){
                                        callback(err);
                                    }
                                    else {
                                        callback(null, foundcard);
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    }, 2 * 1000);  
}

module.exports = callback4;