const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");
const boards = require('./boards.json');

function callback5(callback){
    setTimeout(() => {
        let foundBoard = boards.find((currentBoard) => {
            return currentBoard.name === "Thanos";
        });
        if(typeof foundBoard === 'undefined'){
            console.error("Board not found");
        }
        else {
            callback1(boards, foundBoard.id, (err, foundBoard) => {
                if(err){
                    callback(err);
                } else {
                    callback2(foundBoard.id, (err, foundList) => {
                        if(err){
                            callback(err);
                        }
                        else {
                            let foundMindAndSpace = foundList.filter((currentList) => {
                                return (currentList.name === 'Mind' || currentList.name === 'Space');
                            });

                            if(typeof foundMindAndSpace === 'undefined'){
                                console.error("List not found");
                            }
                            else {
                                foundMindAndSpace.forEach((currentList) => {
                                    callback3(currentList.id, (err, foundcard) => {
                                        if(err){
                                            callback(err);
                                        }
                                        else {
                                            callback(null, foundcard);
                                        }
                                    });
                                });
                            }
                        }
                    });
                }
            });
        }
    }, 2 * 1000);  
}

module.exports = callback5;